# Gitlab Proof For Keyoxide

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account.

For details check out https://keyoxide.org/guides/openpgp-proofs

My Keyoxide profile: https://keyoxide.org/FF61F2A599B030B09FC0DEAD8B8CDBFF362ADD69
